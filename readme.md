Typical maven spring boot application. For details see spring boot amd maven docs.

Example correct `.xml` file for sending emails.

```
<?xml version="1.0" encoding="UTF-8"?>
<emails>
    <config>
        <host>smtp.mailtrap.io</host>
        <port>2525</port>
        <username>890c6aa8765a69</username>
        <password>60b831a29b7da6</password>
        <email-from>890c6aa8765a69-01@inbox.mailtrap.io</email-from>
    </config>
    <email>
        <to>
            <address>to1@test.com</address>
            <address>to2@test.com</address>
        </to>
        <cc>
            <address>cc1@test.com</address>
            <address>cc2@test.com</address>
        </cc>
        <bcc>
            <address>bcc1@test.com</address>
            <address>bcc2@test.com</address>
        </bcc>
        <subject>Subject1</subject>
        <body>
            It's an example message,
            try it out!
        </body>
        <send-time>now</send-time>
    </email>
    <email>
        <to>
            <address>to1@test1.com</address>
            <address>to2@test1.com</address>
        </to>
        <cc>
            <address>cc1@test1.com</address>
            <address>cc2@test1.com</address>
        </cc>
        <bcc>
            <address>bcc1@test1.com</address>
            <address>bcc2@test1.com</address>
        </bcc>
        <subject>Subject2</subject>
        <body>
            It's an example message,
            try it out!
        </body>
        <send-time>2021-12-03T10:15:30</send-time>
    </email>
</emails>
```