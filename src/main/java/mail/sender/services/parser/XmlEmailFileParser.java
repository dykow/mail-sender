package mail.sender.services.parser;

import mail.sender.model.Config;
import mail.sender.model.Email;
import mail.sender.services.storage.StorageService;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class XmlEmailFileParser {
    private final String filePath;

    private ArrayList<Email> emails;
    private Config config;

    Document doc;

    public XmlEmailFileParser(String filePath, StorageService storageService) {
        this.filePath = filePath;
        this.emails = new ArrayList<>();
        config = new Config();
        initXmlDocument(storageService);
    }

    private void initXmlDocument(StorageService storageService) {
        try {
            File file = new File(storageService.load(filePath).toString());
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(file);
            doc.getDocumentElement().normalize();
        } catch (Exception e) {
            throw new RuntimeException("Cannot parse " + e.getMessage());
        }
    }

    public ArrayList<Email> getEmails() {
        return emails;
    }

    public Config getConfig() {
        return config;
    }

    public void parse() {
        parseConfig();
        parseEmails();
    }

    private void parseConfig() {
        Node nConfig = getConfigRoot();
        Element eConfig = (Element) nConfig;

        NodeList host = eConfig.getElementsByTagName("host");
        parseConfigHost(host.item(0));

        NodeList port = eConfig.getElementsByTagName("port");
        parseConfigPort(port.item(0));

        NodeList username = eConfig.getElementsByTagName("username");
        parseConfigUsername(username.item(0));

        NodeList password = eConfig.getElementsByTagName("password");
        parseConfigPassword(password.item(0));

        NodeList from = eConfig.getElementsByTagName("email-from");
        parseConfigEmailFrom(from.item(0));
    }

    private Node getConfigRoot() {
        NodeList root = doc.getElementsByTagName("emails");
        Node nRoot = root.item(0);
        Element eRoot = (Element) nRoot;
        NodeList config = eRoot.getElementsByTagName("config");
        return config.item(0);
    }

    private void parseConfigHost(Node nHost) {
        if (nHost.getNodeType() == Node.ELEMENT_NODE) {
            Element eHost = (Element) nHost;
            String host = eHost.getTextContent();
            config.setHost(host); // TODO switch case on node.getNodeName()
        }
    }

    private void parseConfigPort(Node nPort) {
        if (nPort.getNodeType() == Node.ELEMENT_NODE) {
            Element ePort = (Element) nPort;
            String port = ePort.getTextContent();
            config.setPort(port);
        }
    }

    private void parseConfigUsername(Node nUsername) {
        if (nUsername.getNodeType() == Node.ELEMENT_NODE) {
            Element eUsername = (Element) nUsername;
            String username = eUsername.getTextContent();
            config.setUsername(username);
        }
    }

    private void parseConfigPassword(Node nPassword) {
        if (nPassword.getNodeType() == Node.ELEMENT_NODE) {
            Element ePassword = (Element) nPassword;
            String password = ePassword.getTextContent();
            config.setPassword(password);
        }
    }

    private void parseConfigEmailFrom(Node nFrom) {
        if (nFrom.getNodeType() == Node.ELEMENT_NODE) {
            Element eFrom = (Element) nFrom;
            String emailFrom = eFrom.getTextContent();
            config.setFrom(emailFrom);
        }
    }

    private void parseEmails() {
        NodeList emailList = getEmailList();
        for (int i = 0; i < emailList.getLength(); i++)
        {
            Element eCurrentEmail = getEmail(emailList, i);
            parseEmail(i, eCurrentEmail);
        }
    }

    private NodeList getEmailList() {
        NodeList root = doc.getElementsByTagName("emails");
        Node nRoot = root.item(0);
        Element eRoot = (Element) nRoot;
        return eRoot.getElementsByTagName("email");
    }

    private Element getEmail(NodeList emailList, int i) {
        emails.add(i, new Email());
        Node nEmail = emailList.item(i);
        return (Element) nEmail;
    }

    private void parseEmail(int i, Element eElement) {
        NodeList to = eElement.getElementsByTagName("to");
        parseAddresses(to, i);

        NodeList cc = eElement.getElementsByTagName("cc");
        parseAddresses(cc, i);

        NodeList bcc = eElement.getElementsByTagName("bcc");
        parseAddresses(bcc, i);

        Node subject = eElement.getElementsByTagName("subject").item(0);
        parseSubject(subject, i);

        Node body = eElement.getElementsByTagName("body").item(0);
        parseBody(body, i);

        Node sendTime = eElement.getElementsByTagName("send-time").item(0);
        parseSendTime(sendTime, i);
    }

    private void parseAddresses(NodeList emailsList, int emailIndex) {
        for (int i = 0; i < emailsList.getLength(); i++) {
            Node nEmail = emailsList.item(i);
            if (nEmail.getNodeType() == Node.ELEMENT_NODE)
            {
                addAddresses(emailIndex, (Element) nEmail);
            }
        }
    }

    private void addAddresses(int emailIndex, Element eEmail) {
        NodeList addressList = eEmail.getElementsByTagName("address");
        for (int iter = 0; iter < addressList.getLength(); iter++)
        {
            Node nAddress = addressList.item(iter);
            addAddressToEmail(emailIndex, nAddress);
        }
    }

    private void addAddressToEmail(int emailIndex, Node nAddress) {
        if (nAddress.getNodeType() == Node.ELEMENT_NODE) {
            Element eAddress = (Element) nAddress;

            String addressesType = eAddress.getParentNode().getNodeName();
            String address = eAddress.getTextContent();
            switch (addressesType) {
                case "to" -> emails.get(emailIndex).addToAddress(address);
                case "cc" -> emails.get(emailIndex).addCcAddress(address);
                case "bcc" -> emails.get(emailIndex).addBccAddress(address);
            }
        }
    }

    private void parseSubject(Node nSubject, int emailIndex) {
        if (nSubject.getNodeType() == Node.ELEMENT_NODE) {
            Element eEmail = (Element) nSubject;
            String subject = eEmail.getTextContent();
            addSubjectToEmail(emailIndex, subject);
        }
    }

    private void addSubjectToEmail(int emailIndex, String subject) {
        emails.get(emailIndex).setSubject(subject);
    }

    private void parseBody(Node node, int emailIndex) {
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element eEmail = (Element) node;
            String body = eEmail.getTextContent();
            addBodyToEmail(emailIndex, body);
        }
    }

    private void addBodyToEmail(int emailIndex, String body) {
        emails.get(emailIndex).setBody(body);
    }

    private void parseSendTime(Node nEmail, int emailIndex) {
        if (nEmail.getNodeType() == Node.ELEMENT_NODE) {
            Element eEmail = (Element) nEmail;
            String sendTime = eEmail.getTextContent();
            addSendTimeToEmail(emailIndex, sendTime);
        }
    }

    private void addSendTimeToEmail(int emailIndex, String sendTime) {
        if (sendTime.equals("now")) {
            emails.get(emailIndex).setSendTime(LocalDateTime.now());
        } else {
            emails.get(emailIndex).setSendTime(LocalDateTime.parse(sendTime));
        }
    }
}