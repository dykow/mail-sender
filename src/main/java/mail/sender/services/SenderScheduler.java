package mail.sender.services;

import mail.sender.model.Config;
import mail.sender.model.Email;
import mail.sender.services.parser.XmlEmailFileParser;
import mail.sender.services.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Service
public class SenderScheduler {
    private TaskScheduler taskScheduler;
    private StorageService storageService;

    @Autowired
    public SenderScheduler(TaskScheduler taskScheduler, StorageService storageService) {
        this.taskScheduler = taskScheduler;
        this.storageService = storageService;
    }

    public void scheduleAllFromFile(String filePath) {
        XmlEmailFileParser xmlEmailFileParser = new XmlEmailFileParser(filePath, storageService);
        xmlEmailFileParser.parse();
        List<Email> emails = xmlEmailFileParser.getEmails();
        Config config = xmlEmailFileParser.getConfig();

        for (Email email : emails) {
            Timestamp timestamp = Timestamp.valueOf(email.getSendTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            Date sendTime = new Date(timestamp.getTime());
            taskScheduler.schedule(new Sender(config, email), sendTime);
        }
    }
}
