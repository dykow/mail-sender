package mail.sender.services;

import mail.sender.model.Config;
import mail.sender.model.Email;

import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Sender implements Runnable{

    private final Config config;
    private final Email email;

    private final boolean AUTH = true;
    private final boolean STARTTLS = true;

    public Sender(Config config, Email email) {
        this.config = config;
        this.email = email;

    }

    @Override
    public void run() {
        try {
            send();
        } catch(MessagingException e) {
            System.out.println(e.getMessage());
        }
    }

    private void send() throws MessagingException {
        Message msg = new MimeMessage(setSession(setProperties()));
        setSentDate(msg, email);
        setSubject(msg, email);
        setFrom(msg);
        setTo(msg, email.getTo());
        setCc(msg, email.getCc());
        setBcc(msg, email.getBcc());
        setContent(msg, email);
        Transport.send(msg);
    }

    private Session setSession(Properties props) {
        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(config.getUsername(), config.getPassword());
            }
        });
        return session;
    }

    private Properties setProperties() {
        Properties props = new Properties();

        props.put("mail.smtp.port", config.getPort());
        props.put("mail.smtp.host", config.getHost());
        props.put("mail.smtp.auth", AUTH);
        props.put("mail.smtp.starttls.enable", STARTTLS);

        return props;
    }

    private void setSentDate(Message msg, Email email) throws MessagingException {
        msg.setSentDate(new Date());
    }

    private void setSubject(Message msg, Email email) throws MessagingException {
        msg.setSubject(email.getSubject());
    }

    private void setFrom(Message msg) throws MessagingException {
        msg.setFrom(new InternetAddress(config.getFrom(), false));
    }

    private void setTo(Message msg, ArrayList<String> to) throws MessagingException {
        StringBuilder builder = new StringBuilder();
        to.forEach((val)-> builder.append(val + ","));
        String toAddressesList = builder.toString();
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toAddressesList));
    }

    private void setCc(Message msg, ArrayList<String> cc) throws MessagingException {
        StringBuilder builder = new StringBuilder();
        cc.forEach((val)-> builder.append(val + ","));
        String ccAddressesList = builder.toString();
        msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccAddressesList));
    }

    private void setBcc(Message msg, ArrayList<String> bcc) throws MessagingException {
        StringBuilder builder = new StringBuilder();
        bcc.forEach((val)-> builder.append(val + ","));
        String bccAddressesList = builder.toString();
        msg.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bccAddressesList));
    }

    private void setContent(Message msg, Email email) throws MessagingException {
        msg.setContent(email.getBody(), "text/html");
    }
}