package mail.sender.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Email {
    private ArrayList<String> to;
    private ArrayList<String> cc;
    private ArrayList<String> bcc;
    private String subject;
    private String body;
    private LocalDateTime sendTime;

    public Email() {
        to = new ArrayList<>();
        cc = new ArrayList<>();
        bcc = new ArrayList<>();
        subject = new String();
        body = new String();
        sendTime = null;
    }

    public Email(ArrayList<String> doAddress, ArrayList<String> dwAddress, ArrayList<String> udwAddress, String subject, String body, LocalDateTime sendTime) {
        this.to = doAddress;
        this.cc = dwAddress;
        this.bcc = udwAddress;
        this.subject = subject;
        this.body = body;
        this.sendTime = sendTime;
    }

    public void addToAddress(String s) {
        to.add(s);
    }

    public void addCcAddress(String s) {
        cc.add(s);
    }

    public void addBccAddress(String s) {
        bcc.add(s);
    }

    public void setTo(ArrayList<String> to) {
        this.to = to;
    }

    public void setCc(ArrayList<String> cc) {
        this.cc = cc;
    }

    public void setBcc(ArrayList<String> bcc) {
        this.bcc = bcc;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setSendTime(LocalDateTime sendTime) {
        this.sendTime = sendTime;
    }

    public ArrayList<String> getTo() {
        return to;
    }

    public ArrayList<String> getCc() {
        return cc;
    }

    public ArrayList<String> getBcc() {
        return bcc;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public LocalDateTime getSendTime() {
        return sendTime;
    }
}
